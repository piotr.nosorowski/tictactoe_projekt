import pygame
import os
import numpy
import pygame.freetype

pygame.init()

height = 750
width = 500
#colors
white = (255,255,255)
black = (0,0,0)
#line thiccness
thiccness = 5
#grid definition
grid_rows = 3
grid_cols = 3
#mouse
mouse_x_pos = 0
mouse_y_pos = 0
player = 1

window_display = pygame.display.set_mode((width,height))
pygame.display.set_caption("Kółko i Florczyk")

circle_image_asset = pygame.image.load(os.path.join('assets', 'circle.png'))
circle_asset = pygame.transform.scale(circle_image_asset, (100, 100))

cross_image_asset = pygame.image.load(os.path.join('assets', 'cross.png'))
cross_asset = pygame.transform.scale(cross_image_asset, (100, 100))

background = pygame.image.load(os.path.join('assets', 'bg.png'))

#main ttt grid
ttt_grid = numpy.zeros((grid_rows,grid_cols))

#drawing

def grid_draw():
    #higher line
    pygame.draw.line(window_display,black,(200,300),(200,600),thiccness)
    pygame.draw.line(window_display,black,(300,300),(300,600),thiccness)
    
    #bottom line
    pygame.draw.line(window_display,black,(100,400),(400,400),thiccness)
    pygame.draw.line(window_display,black,(100,500),(400,500),thiccness)


def window_draw():
    window_display.blit(background, (0, 0))
    grid_draw()
    
    #
    #window_display.blit(cross_asset,(100,400))
    
    pygame.display.update()


#gameplay functions

def ttt_click(row,col,player):
    ttt_grid[row][col] = player

def ttt_isavalible(row,col):
    if ttt_grid[row][col] == 0:
        return True
    else:
        return False

def ttt_isgridfull():
    for row in range(grid_rows):
        for col in range(grid_cols):
            if ttt_grid[row][col] == 0:
                return False
    return True

def ttt_isgameended(player):

    for col in range(grid_cols):
        if ttt_grid[0][col] == player and ttt_grid[1][col] == player and ttt_grid[2][col] == player:
            print("Player " + str(player) + " WINS!")
            return True
 
    for row in range(grid_rows):
        if ttt_grid[row][0] == player and ttt_grid[row][1] == player and ttt_grid[row][2] == player:
                print("Player " + str(player) + " WINS!")
                return True

    if ttt_grid[2][0] == player and ttt_grid[1][1] == player and ttt_grid[0][2] == player:
        print("Player " + str(player) + " WINS!")
        return True
    
    if ttt_grid[0][0] == player and ttt_grid[1][1]  == player and ttt_grid[2][2] == player:
        print("Player " + str(player) + " WINS!")
        return True
    
    return False

def ttt_placemarker(list_clicked,player):

    #1
    if list_clicked == [0,0]:
        if player == 1:
            window_display.blit(circle_asset,(100,300)) 
        elif player == 2:
            window_display.blit(cross_asset,(100,300)) 
    #2
    elif list_clicked == [0,1]:
        if player == 1:
            window_display.blit(circle_asset,(200,300)) 
        elif player == 2:
            window_display.blit(cross_asset,(200,300))
    #3
    elif list_clicked == [0,2]:
        if player == 1:
            window_display.blit(circle_asset,(300,300)) 
        elif player == 2:
            window_display.blit(cross_asset,(300,300))
    #4 
    elif list_clicked == [1,0]:
        if player == 1:
            window_display.blit(circle_asset,(100,400)) 
        elif player == 2:
            window_display.blit(cross_asset,(100,400))
    #5
    elif list_clicked == [1,1]:
        if player == 1:
            window_display.blit(circle_asset,(200,400)) 
        elif player == 2:
            window_display.blit(cross_asset,(200,400))
    #6
    elif list_clicked == [1,2]:
        if player == 1:
            window_display.blit(circle_asset,(300,400)) 
        elif player == 2:
            window_display.blit(cross_asset,(300,400))
    #7
    elif list_clicked == [2,0]:
        if player == 1:
            window_display.blit(circle_asset,(100,500)) 
        elif player == 2:
            window_display.blit(cross_asset,(100,500))
    #8
    elif list_clicked == [2,1]:
        if player == 1:
            window_display.blit(circle_asset,(200,500)) 
        elif player == 2:
            window_display.blit(cross_asset,(200,500))
    #9
    elif list_clicked == [2,2]:
        if player == 1:
            window_display.blit(circle_asset,(300,500)) 
        elif player == 2:
            window_display.blit(cross_asset,(300,500))
    

def ttt_click_locate(mouse_x_pos,mouse_y_pos):
    list_clicked = []
    #1
    if (mouse_x_pos > 100 and mouse_x_pos < 200) and (mouse_y_pos > 300 and mouse_y_pos < 400):
        list_clicked = [0,0]
    #2
    if (mouse_x_pos > 200 and mouse_x_pos < 300) and (mouse_y_pos > 300 and mouse_y_pos < 400):
        list_clicked = [0,1]
    #3
    if (mouse_x_pos > 300 and mouse_x_pos < 400) and (mouse_y_pos > 300 and mouse_y_pos < 400):
        list_clicked = [0,2]
    #4
    if (mouse_x_pos > 100 and mouse_x_pos < 200) and (mouse_y_pos > 400 and mouse_y_pos < 500):
        list_clicked = [1,0]
    #5
    if (mouse_x_pos > 200 and mouse_x_pos < 300) and (mouse_y_pos > 400 and mouse_y_pos < 500):
        list_clicked = [1,1]
    #6
    if (mouse_x_pos > 300 and mouse_x_pos < 400) and (mouse_y_pos > 400 and mouse_y_pos < 500):
        list_clicked = [1,2]
    #7
    if (mouse_x_pos > 100 and mouse_x_pos < 200) and (mouse_y_pos > 500 and mouse_y_pos < 600):
        list_clicked = [2,0]
    #8
    if (mouse_x_pos > 200 and mouse_x_pos < 300) and (mouse_y_pos > 500 and mouse_y_pos < 600):
        list_clicked = [2,1]
    #9
    if (mouse_x_pos > 300 and mouse_x_pos < 400) and (mouse_y_pos > 500 and mouse_y_pos < 600):
        list_clicked = [2,2]        
    
    
    return list_clicked
#main



def main():
    ttt_isworking = True
    player = 1
    mousex = 0
    mousey = 0
    while ttt_isworking == True:
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                ttt_isworking = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            mousex = event.pos[0] #hehe
            mousey = event.pos[1]
            
            clicked_row = ttt_click_locate(mousex,mousey)[0]
            clicked_col = ttt_click_locate(mousex,mousey)[1]

            if ttt_isavalible(clicked_row,clicked_col):
                if player == 1:
                    ttt_click(clicked_row,clicked_col, 1 )
                    ttt_isgameended(player)
                    player = 2
                elif player == 2:
                    ttt_click(clicked_row,clicked_col, 2 )
                    ttt_isgameended(player)
                    player = 1
                
                ttt_placemarker(ttt_click_locate(mousex,mousey),player)
                pygame.display.update()
                print(ttt_grid)
        
        
        
        
        
        window_draw()

main()